<?php

/**
* Implementation of hook_rules_event_info().
*/
function node_access_rules_rules_event_info() {
  $events = array();

  $events['node_access_rules_edit'] = array(
    'label' => t('Trying to edit a Node'),
    'group' => t('Content Access Grant'),
    'variables' => array(
      'node' => array(
        'type' => 'node',
        'label' => t('Node to edit')
      ),
    )
  );

  $events['node_access_rules_delete'] = array(
    'label' => t('Trying to delete a Node'),
    'group' => t('Content Access Grant'),
    'variables' => array(
      'node' => array(
        'type' => 'node',
        'label' => t('Node to delete')
      ),
    )
  );

  return $events;
	
}

/**
 * Implements hook_rules_action_info().
 */
function node_access_rules_rules_action_info() {
  $actions = array();

  $actions['node_access_rules_allow_edit'] = array(
    'label' => t('Allow editing of Node'),
    'group' => t('Content Access Grant'),
    'base' => 'node_access_rules_allow_edit',
    'callbacks' => array(
      'execute' => 'node_access_rules_action_allow_edit',
    ),
  );

  $actions['node_access_rules_deny_edit'] = array(
    'label' => t('Do not allow editing of Node'),
    'group' => t('Content Access Grant'),
    'base' => 'node_access_rules_deny_edit',
    'callbacks' => array(
      'execute' => 'node_access_rules_action_deny_edit',
    ),
  );
  
  $actions['node_access_rules_allow_delete'] = array(
    'label' => t('Allow deleting of Node'),
    'group' => t('Content Access Grant'),
    'base' => 'node_access_rules_allow_delete',
    'callbacks' => array(
      'execute' => 'node_access_rules_action_allow_delete',
    ),
  );
  
  $actions['node_access_rules_deny_delete'] = array(
    'label' => t('Do not allow deleting of Node'),
    'group' => t('Content Access Grant'),
    'base' => 'node_access_rules_deny_delete',
    'callbacks' => array(
      'execute' => 'node_access_rules_action_deny_delete',
    ),
  );
  
  return $actions;
}

function node_access_rules_action_allow_edit() {
  global $node_access_rules;
  $node_access_rules = NODE_ACCESS_ALLOW;
}

function node_access_rules_action_deny_edit() {
  global $node_access_rules;
  $node_access_rules = NODE_ACCESS_DENY;
}

function node_access_rules_action_allow_delete() {
  global $node_access_rules;
  $node_access_rules = NODE_ACCESS_ALLOW;
}

function node_access_rules_action_deny_delete() {
  global $node_access_rules;
  $node_access_rules = NODE_ACCESS_DENY;
}